
----Проверка окна корректировок----

set nocount on
use test2

---- 1 - задаем номер релиза

if OBJECT_ID ('test2..OKNO_id_release')is not null drop table test2..OKNO_id_release
create table test2..OKNO_id_release
(ora_id_release_rule varchar (300)
)
insert into test2..OKNO_id_release

select '425286272122'  -- номер окна
--	select * from test2..OKNO_id_release

---- 2 - выгружаем АЦА

if OBJECT_ID ('test2..OKNO_ACA')is not null drop table test2..OKNO_ACA
create table test2..OKNO_ACA
(ora_id_groupart int
,ora_id_type_matrix int
,ora_id_art int
,ora_id_rule_uw bigint
,rang int
,ACA varchar (300)
,Smena_grp int 
,ora_id_release_rule varchar (300)
)
insert into test2..OKNO_ACA

select distinct 
 a1.ora_id_groupart
,a1.ora_id_type_matrix 
,a1.ora_id_art
,a1.ora_id_rule_uw 
,a1.rank
,'aca' as aca
,case when a1.ora_id_groupart = a3.ora_id_groupart then 0 else 1 end as Smena_grp
,a1.ora_id_release_rule
--select top 1 *
from TABLE_ACA a1 --АЦА черновик
join TABLE_article_vw a3      on a3.ora_id_art = a1.ora_id_art
join test2..OKNO_id_release g on g.ora_id_release_rule = a1.ora_id_release_rule
where a1.dt_end is  null
--            select count(a.ora_id_rule_uw) from test2..OKNO_ACA a 

----- 2.1 - получаем ТТ в ОМ-----------------------------
if OBJECT_ID ('test2..OKNO_TT_OM')is not null drop table test2..OKNO_TT_OM
create table test2..OKNO_TT_OM
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,rule_uw_name varchar (300) 
,ora_id_rule_uw bigint
)
insert into test2..OKNO_TT_OM

select distinct  
 a1.ora_id_ws
,b.ora_id_groupart
,b.ora_id_type_matrix 
,b.name
,a1.ora_id_rule_uw 
from Table_OM_TT a1 --ТТ в ОМ
join Table_OM b on b.ora_id_rule_uw = a1.ora_id_rule_uw and b.dt_end is null --ОМ
join (select distinct ora_id_rule_uw from test2..OKNO_ACA) t on t.ora_id_rule_uw = a1.ora_id_rule_uw
where a1.dt_end is null
    and cast(getdate() as date) between a1.date_begin and isnull(a1.date_end,cast(GETDATE() as date))
--            select count (a.ora_id_ws) from test2..OKNO_TT_OM a

---- 2.2 - разворот АЦА до ТТ
if OBJECT_ID ('test2..OKNO_ACA_TP_TT')is not null drop table test2..OKNO_ACA_TP_TT
create table test2..OKNO_ACA_TP_TT
(ora_id_ws                int
,ora_id_groupart          int
,ora_id_type_matrix       int
,ora_id_art               int
,rule_uw_name             varchar (300) 
,ora_id_rule_uw           bigint
,rang                     int
,ACA                      varchar (300)
,Smena_grp                int 
,ora_id_release_rule      varchar (300)
)
insert into test2..OKNO_ACA_TP_TT

select distinct 
 b.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
,a1.ora_id_art
,b.rule_uw_name
,a1.ora_id_rule_uw 
,a1.rang
,'aca' as aca
,a1.Smena_grp
,a1.ora_id_release_rule
from test2..OKNO_ACA a1
join test2..OKNO_TT_OM b on b.ora_id_rule_uw = a1.ora_id_rule_uw
--            select count (a.ora_id_ws) from test2..OKNO_ACA_TP_TT a

------ 2.3 - АЦА. Считаем кол-во ТП в ОМ------------------------
if OBJECT_ID ('test2..OKNO_ACA_TP')is not null drop table test2..OKNO_ACA_TP
create table test2..OKNO_ACA_TP
(ora_id_rule_uw bigint
,ART float
)
insert into test2..OKNO_ACA_TP
select distinct 
 a.ora_id_rule_uw
,count (distinct a.ora_id_art)
from test2..OKNO_ACA a
group by  a.ora_id_rule_uw 

---- 3 - Выгружаем CMS
IF OBJECT_ID('test2..OKNO_cms_ethalon') IS NOT NULL DROP TABLE test2..OKNO_cms_ethalon;
create table test2..OKNO_cms_ethalon
( ora_id_ws          int
, ora_id_groupart    int
, ora_id_type_matrix int
, klaster            int
, etalon             float     
)
insert into test2..OKNO_cms_ethalon

select a1.ora_id_ws, a1.ora_id_groupart, a1.ora_id_type_matrix, c.clstr, sum (a1.ora_etalon)
from TABLE_etalon a1 --эталон
join test2..OKNO_TT_OM a3 on a3.ora_id_groupart = a1.ora_id_groupart
                         and a3.ora_id_ws = a1.ora_id_ws
                         and a3.ora_id_type_matrix = a1.ora_id_type_matrix
join dev_db..v_cms_matrix_aps_tbl c on c.ora_id_matrix = a1.ora_id_matrix and c.dt_end is null
where a1.ora_etalon >0
group by a1.ora_id_ws, a1.ora_id_groupart, a1.ora_id_type_matrix, c.clstr
--            select count (a.ora_id_ws) from test2..OKNO_cms_ethalon a

------ 4 - СУАГ ДА ТТ -------------------------------------

--4.0 - получаем ДА где не указан ТТ и добавляем признак all_stores
if OBJECT_ID ('test2..OKNO_TT_DATT0')is not null drop table test2..OKNO_TT_DATT0
create table test2..OKNO_TT_DATT0
(ora_id_ws int
,ora_id_art int 
,all_stores varchar(10)
)
insert into test2..OKNO_TT_DATT0

select distinct
 a1.ora_id_ws
,a1.ora_id_art
,'y' as all_stores
from TABLE_DA a1 --ДА
where a1.dt_end is null
and a1.ora_id_ws is null               -- null значит подача на всю сеть
--            select count (a.ora_id_art) from test2..OKNO_TT_DATT0 a

--4.1 - в список ТТ добавляем признак all_stores
if OBJECT_ID ('test2..OKNO_TT_DATT1')is not null drop table test2..OKNO_TT_DATT1
create table test2..OKNO_TT_DATT1
(ora_id_ws int
,all_stores varchar(10)
)
insert into test2..OKNO_TT_DATT1

select distinct
a.ora_id_ws
,'y' as all_stores
from TABLE_store_tbl a --
where a.dt_end is null
and a.is_active = 1

--4.2 - разворачиваем ДГ ТП на всю сеть до ТТ и обьединяем с ТТ -ТП где не было разворота, оба блока ограничиваем по ТТ-ТГ из ОМ

if OBJECT_ID ('test2..OKNO_TT_DATT2')is not null drop table test2..OKNO_TT_DATT2
create table test2..OKNO_TT_DATT2
(ora_id_ws int
,ora_id_groupart int
,ora_id_art int 
,koefAZ float
)
insert into test2..OKNO_TT_DATT2

                select distinct
                c.ora_id_ws
                ,b.ora_id_groupart
                ,a1.ora_id_art
                ,case when a2.td_frmt_id = 1 then 1
                                 when a2.td_frmt_id = 2 then 6
                                 when a2.td_frmt_id = 3 then 0.4
                                 when a2.td_frmt_id = 104 then 1 
                                 when a2.td_frmt_id = 47 then 0.01 else 0 end as koefAZ              
                from Table_assort2_tbl a1  --
                join test2..OKNO_TT_DATT0 a on a.ora_id_art = a1.ora_id_art
                join test2..OKNO_TT_DATT1 c on c.all_stores = a.all_stores
                join TABLE_article_vw b on b.ora_id_art = a1.ora_id_art
                join test2..OKNO_TT_OM e on e.ora_id_ws = c.ora_id_ws
                                        and e.ora_id_groupart = b.ora_id_groupart
                join dev_db..whs_vw a2  on a2.ora_id_ws = c.ora_id_ws
                where a1.dt_end is null
union
                select distinct
                 a1.ora_id_ws
                ,b.ora_id_groupart
                ,a1.ora_id_art
                ,case when a2.td_frmt_id = 1 then 1
                                 when a2.td_frmt_id = 2 then 6
                                 when a2.td_frmt_id = 3 then 0.4
                                 when a2.td_frmt_id = 104 then 1 
                                 when a2.td_frmt_id = 47 then 0.01 else 0 end as koefAZ  
                from Table_assort2_tbl a1
                join TABLE_whs_vw a2    on a2.ora_id_ws = a1.ora_id_ws
                join TABLE_article_vw b on b.ora_id_art = a1.ora_id_art
                join test2..OKNO_TT_OM e on e.ora_id_ws = a1.ora_id_ws
                                        and e.ora_id_groupart = b.ora_id_groupart
                where a1.dt_end is null
--            select count (a.ora_id_ws) from test2..OKNO_TT_DATT2 a

------- 4.3 - ДА считаем кол-во ТП на ТТ-----------------------
if OBJECT_ID ('test2..OKNO_DATT_TP')is not null drop table test2..OKNO_DATT_TP
create table test2..OKNO_DATT_TP
(ora_id_ws int
,ora_id_groupart int
,TP float 
)
insert into test2..OKNO_DATT_TP

select distinct
a1.ora_id_ws   ,a1.ora_id_groupart       --,a1.td_matrix_type_id
,count (distinct a1.ora_id_art)
from test2..OKNO_TT_DATT2 a1
group by 
 a1.ora_id_ws   ,a1.ora_id_groupart       --,a1.td_matrix_type_id

---------- 5 - ПА по номеру релиза  ---------------------------------------
if OBJECT_ID ('test2..OKNO_PA_chernovik') is not null  drop table test2..OKNO_PA_chernovik
create table test2..OKNO_PA_chernovik (ora_id_art int, ora_id_controwner int, ora_id_ws int, ora_id_type_matrix int, ora_id_groupart int ,koefAZ float
)
insert into test2..OKNO_PA_chernovik
select a.ora_id_art, a.ora_id_controwner, a.ora_id_ws, a.ora_id_type_matrix, a.ora_id_groupart
                ,case when a2.td_frmt_id = 1 then 1
                      when a2.td_frmt_id = 2 then 6
                      when a2.td_frmt_id = 3 then 0.4
                      when a2.td_frmt_id = 104 then 1 
                      when a2.td_frmt_id = 47 then 0.01 else 0 end as koefAZ  
from TABLE_PA a with(nolock)  --ПА
join (select distinct a.ora_id_agr_target_assort 
                  from TABLE_aca_draft_tbl a 
                  join test2..OKNO_id_release b on b.ora_id_release_rule = a.ora_id_release_rule) b on b.ora_id_agr_target_assort = a.ora_id_agr_target_assort
join dev_db..whs_vw a2 on a2.ora_id_ws = a.ora_id_ws
where dt_end is null;
--            select count (a.ora_id_ws) from test2..OKNO_PA_chernovik a

--------- 5.1 - ПА считаем ТП ---------------------------------------
if OBJECT_ID ('test2..OKNO_PA_chernovik_TP')is not null drop table test2..OKNO_PA_chernovik_TP
create table test2..OKNO_PA_chernovik_TP
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,TPinPA float 
)
insert into test2..OKNO_PA_chernovik_TP

select distinct
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
,count (a1.ora_id_art)
from test2..OKNO_PA_chernovik a1
group by 
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
--            select count (a.ora_id_ws) from test2..OKNO_PA_chernovik_TP a

--------- 5.2 - ПА считаем ТП с КА ---------------------------------------
if OBJECT_ID ('test2..OKNO_PA_chernovik_TP_sKA')is not null drop table test2..OKNO_PA_chernovik_TP_sKA
create table test2..OKNO_PA_chernovik_TP_sKA
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,TPinPA_sKA float 
)
insert into test2..OKNO_PA_chernovik_TP_sKA

select distinct
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
,count (a1.ora_id_art)
from test2..OKNO_PA_chernovik a1
where a1.ora_id_controwner is not null
group by 
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
--            select count (a.ora_id_ws) from test2..OKNO_PA_chernovik_TP_sKA a

---------- 6 - Объединение ДА и АЦА--------------------------
if OBJECT_ID ('test2..OKNO_DA_ACA')is not null drop table test2..OKNO_DA_ACA
create table test2..OKNO_DA_ACA
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,ora_id_art int
,rule_uw_name varchar (300) 
,ora_id_rule_uw bigint
,aca varchar (300) 
,koefAZ float
,rang int
,Smena_grp int
)
insert into test2..OKNO_DA_ACA

select distinct
 coalesce(a.ora_id_ws, b.ora_id_ws)
,coalesce(a.ora_id_groupart, b.ora_id_groupart)
,b.ora_id_type_matrix
,coalesce(a.ora_id_art, b.ora_id_art)
,b.rule_uw_name
,b.ora_id_rule_uw 
,b.ACA
,a.koefAZ
,b.rang
,b.Smena_grp
from test2..OKNO_TT_DATT2 a
full join test2..OKNO_ACA_TP_TT b on b.ora_id_ws = a.ora_id_ws
                                 and b.ora_id_art = a.ora_id_art
								 and b.ora_id_groupart = a.ora_id_groupart
-- select count (a.ora_id_ws) from test2..OKNO_DA_ACA a

----------7 - Объединение ДА и АЦА + ПА + Эталон --------------------------
if OBJECT_ID ('test2..OKNO_DA_ACA_PA_CMS')is not null drop table test2..OKNO_DA_ACA_PA_CMS
create table test2..OKNO_DA_ACA_PA_CMS
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,ora_id_art int
,rule_uw_name varchar (300) 
,ora_id_rule_uw bigint
,koefAZ float
,inDA int
,inACA int
,inPA int
,inPAsKA int
,etalon int
,rang int
,klaster int
,Smena_grp int)

insert into test2..OKNO_DA_ACA_PA_CMS

select distinct
 a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,a.ora_id_art
,case when a.rule_uw_name is not null then a.rule_uw_name else b2.rule_uw_name end as rule_uw_name
,case when a.ora_id_rule_uw  is not null then a.ora_id_rule_uw  else b2.ora_id_rule_uw  end as ora_id_rule_uw 
,a.koefAZ
,case when a.koefAZ is not null then 1 else 0 end as inDA
,case when a.aca is not null then 1 else 0 end as inACA
,case when b.ora_id_art is not null then 1 else 0 end as inPA
,case when b1.ora_id_art is not null then 1 else 0 end as inPAsKA
,c.etalon
,a.rang
,c.klaster
,a.Smena_grp
from test2..OKNO_DA_ACA a
left join test2..OKNO_PA_chernovik b on b.ora_id_ws = a.ora_id_ws
                                    and b.ora_id_art = a.ora_id_art
									and b.ora_id_groupart = a.ora_id_groupart
                                    and b.ora_id_type_matrix = a.ora_id_type_matrix
left join test2..OKNO_PA_chernovik b1 on b1.ora_id_ws = a.ora_id_ws
                                     and b1.ora_id_art = a.ora_id_art
                                     and b1.ora_id_groupart = a.ora_id_groupart
                                     and b1.ora_id_type_matrix = a.ora_id_type_matrix and b1.ora_id_controwner is not null
left join test2..OKNO_cms_ethalon c on c.ora_id_ws = a.ora_id_ws
                                   and c.ora_id_groupart = a.ora_id_groupart
                                   and c.ora_id_type_matrix = a.ora_id_type_matrix
left join test2..OKNO_ACA_TP_TT b2 on b2.ora_id_ws = a.ora_id_ws
                                  and b2.ora_id_groupart = a.ora_id_groupart
                                  and b2.ora_id_type_matrix = a.ora_id_type_matrix
where a.ora_id_type_matrix is not null
--            select count (a.ora_id_ws) from test2..OKNO_DA_ACA_PA_CMS a

-----------8 - СУАГ ДА-АЦА считаем максимальный  эталон -------------------------------------
if OBJECT_ID ('test2..OKNO_TTnaGeoEtalonMax')is not null drop table test2..OKNO_TTnaGeoEtalonMax
create table test2..OKNO_TTnaGeoEtalonMax
(ora_id_rule_uw bigint
,etalonTTnaGeo INT
)
insert into test2..OKNO_TTnaGeoEtalonMax

select 
 a1.ora_id_rule_uw 
,max (a1.etalon)
from test2..OKNO_DA_ACA_PA_CMS a1
group by
a1.ora_id_rule_uw

-------------------------------------------------------------Уровень ОМа------------------------------------------------------------------------------
--Скрипт с ограничением удалённых ТМ
if OBJECT_ID ('test2..OKNO_chern_UrovenOM_TM')is not null drop table test2..OKNO_chern_UrovenOM_TM
create table test2..OKNO_chern_UrovenOM_TM
(ora_id_rule_uw bigint
       );
insert into test2..OKNO_chern_UrovenOM_TM

select distinct ora_id_rule_uw
from AVTOSources.suag.suag_rule_uw_ws_aps_tbl r
join dev_db..whs_tbl w on w.dt_end is null
       and w.ora_id_ws = r.ora_id_ws
       and w.working = 1
where r.dt_end is null;

--селект
if OBJECT_ID ('test2..OKNO_chern_UrovenOM')is not null drop table test2..OKNO_chern_UrovenOM
create table test2..OKNO_chern_UrovenOM
(ora_id_rule_uw bigint
,lvl_geo varchar (300)
       );
insert into test2..OKNO_chern_UrovenOM

select rul.ora_id_rule_uw
             , case when rul.lvl_priority_id= 1 then 'МХ' 
                    when rul.lvl_priority_id =2 then 'Матрица' 
                    when rul.lvl_priority_id =3 then 'Город' 
                    when rul.lvl_priority_id =4 then 'Филиал'
                    when rul.lvl_priority_id =5 then 'РЦ' 
                    when rul.lvl_priority_id =6 then 'Вся сеть' 
            		end as lvl_geo
from (select rul.ora_id_rule_uw
                    ,rul.ora_id_type_matrix
                    ,rul.ora_id_groupart
                    ,rul.name
                    ,min(rul.lvl_priority_id) lvl_priority_id
       from   (select 
                            rul.ora_id_rule_uw
                           ,rul.ora_id_type_matrix
                           ,rul.ora_id_groupart
                           ,rul.name
                           ,case when rul.ora_id_ws is not null then 1
                                        when rul.ora_id_matrix is not null then 2
                                        when rul.ora_id_geosite is not null then 3
                                        when rul.ora_id_filial is not null then 4
                                        when rul.ora_id_contr is not null then 5
                                        else 6
                           end as lvl_priority_id
                    from TABLE_rule2_tbl rul
                    where 1= 1
                                  and rul.dt_end is null
                                  and rul.aps_is_del =0
                                  and rul.aps_is_test = 0 -- только "боевые ОМы"
                                  and rul.ora_id_rule_uw in (select ora_id_rule_uw from test2..OKNO_chern_UrovenOM_TM) -- ОМ, к которым привязаны действующие ТТ
                    )rul
             group by rul.ora_id_rule_uw
                    ,rul.ora_id_type_matrix
                    ,rul.ora_id_groupart
                    ,rul.name
       )rul
join dev_db..v_cms_type_matrix_aps_tbl m on m.dt_end is null
       and m.ora_id_type_matrix = rul.ora_id_type_matrix
       and m.isdelete = 0

-------------------------------------------------------------- - Список открывающихся магазинов НА НОВОЙ ГЕО---------------------------------------------------------------------
IF OBJECT_ID('test2..OKNO_whs_open') IS NOT NULL     DROP TABLE test2..OKNO_whs_open;
create table test2..OKNO_whs_open
(ora_id_ws int
,ora_dt_start date
)

insert into test2..OKNO_whs_open

select distinct a1.ora_id_ws, a1.ora_dt_start--, a1.condition_name--, a2.*
from TABLE_division_state_vw a1
join TABLE_whs_vw a2 on a1.td_whs_id = a2.td_whs_id
where a1.condition_name in ('2.1. Функционирует')--, '8.3. Редизайн: Новый стиль')
    and a1.ora_dt_start between cast(getdate()+1 as date) and cast(getdate()+31 as date)
    and a1.ora_dt_end is null
                --Список работающих магазинов

insert into test2..OKNO_whs_open
select distinct a1.ora_id_ws, a1.ora_dt_start--, a1.condition_name
from TABLE_division_state_vw a1
join dev_db..whs_vw a2 on a1.td_whs_id = a2.td_whs_id and a2.working = 1
where a1.condition_name in ('2.1. Функционирует', '8.3. Редизайн: Новый стиль')
                and a2.id_subfrmt not in (61,65,67,37)   --Исключаем ОПТ, УМ, Почта 
                
--Пытаемся получить СУАГжный филиал

IF OBJECT_ID('test2..OKNO_branch_whs_id') IS NOT NULL
DROP TABLE test2..OKNO_branch_whs_id;
create table test2..OKNO_branch_whs_id
(branch_name varchar (64)
, frmt varchar (64)
, whs_branch_by_kxd varchar (64)
, ora_id_ws int
)
insert into test2..OKNO_branch_whs_id

select distinct  a1.branch_name,a1.frmt,a2.whs_name,a2.ora_id_ws
from TABLE_whs_vw a1
join TABLE_warehouse_vw a2 on a1.branch_name =a2.whs_name
where frmt= 'МД'

union
select distinct  a1.branch_name,a1.frmt,a2.whs_name,a2.ora_id_ws
from TABLE_whs_vw a1
join TABLE_warehouse_vw a2 on a1.branch_name + ' ГМ' =a2.whs_name
where frmt= 'ГМ'

union
select distinct  a1.branch_name,a1.frmt,a2.whs_name,a2.ora_id_ws
from TABLE_whs_vw a1
join TABLE_warehouse_vw a2 on a1.branch_name + ' МК' =a2.whs_name
where frmt= 'МК'

union
select distinct a1.branch_name,a1.frmt,a2.whs_name,a2.ora_id_ws
from TABLE_whs_vw a1
join TABLE_warehouse_vw a2 on a1.branch_name + ' МА' =a2.whs_name
where frmt= 'МА'

--Получаем базу магазинов с геосайтом и суажным филиалом
IF OBJECT_ID('test2..OKNO_chern_whs_suag') IS NOT NULL
DROP TABLE test2..OKNO_chern_whs_suag;

create table test2..OKNO_chern_whs_suag
( ora_id_ws int
, id_frmt int
, ora_id_format int
, ora_id_dc  int
, ora_id_geosite int
, name_geosite varchar (64)
, ora_id_branch_by_kxd int
, whs_branch_by_kxd varchar (64)
, city_name varchar (64)
, td_city_id int
, branch_name varchar (64)
, td_branch_id int
)

insert into test2..OKNO_chern_whs_suag
select distinct a1.ora_id_ws, a1.id_frmt, a1.ora_id_format, a1.ora_id_dc ,a3.ora_id_geosite, a3.name_geosite, a4.ora_id_ws, a4.whs_branch_by_kxd, a1.city_name, a1.td_city_id, a1.branch_name, a1.td_branch_id
from TABLE_whs_vw a1
left join TABLE_warehouse_vw a2 on a1.ora_id_ws=a2.ora_id_ws
left join TABLE_whs_geosite_vw a3 on a2.ora_id_geosite=a3.ora_id_geosite
left join test2..OKNO_branch_whs_id a4 on a1.branch_name = a4.branch_name and a1.frmt = a4.frmt
join test2..OKNO_whs_open a5 on a1.ora_id_ws = a5.ora_id_ws
--where a1.td_frmt_id = 1 --and  a3.name_geosite = 'Астрахань'

--считаем количество работающих магазинов
IF OBJECT_ID('test2..OKNO_chern_whs_working') IS NOT NULL
DROP TABLE test2..OKNO_chern_whs_working;

create table test2..OKNO_chern_whs_working
( ora_id_branch_by_kxd int
, ora_id_geosite int
, count_whs_id int
)
insert into test2..OKNO_chern_whs_working

select a1.ora_id_branch_by_kxd, a1.ora_id_geosite, count(a1.ora_id_ws) from test2..OKNO_chern_whs_suag a1
join dev_db..whs_Vw a2 on a1.ora_id_ws = a2.ora_id_ws and a2.working = 1
group by a1.ora_id_branch_by_kxd, a1.ora_id_geosite

--считаем количество не работающих магазинов
IF OBJECT_ID('test2..OKNO_chern_whs_not_working') IS NOT NULL
DROP TABLE test2..OKNO_chern_whs_not_working;

create table test2..OKNO_chern_whs_not_working
( ora_id_branch_by_kxd int
, ora_id_geosite int
, count_whs_id int
)

insert into test2..OKNO_chern_whs_not_working
select a1.ora_id_branch_by_kxd, a1.ora_id_geosite, count(a1.ora_id_ws) from test2..OKNO_chern_whs_suag a1
join dev_db..whs_Vw a2 on a1.ora_id_ws = a2.ora_id_ws and a2.working = 0
group by a1.ora_id_branch_by_kxd, a1.ora_id_geosite

--Скрещиваем 2 таблицы и находим закрытые магазины по которым нет географии с открытыми
IF OBJECT_ID('test2..OKNO_chern_problem_geo') IS NOT NULL
DROP TABLE test2..OKNO_chern_problem_geo;

create table test2..OKNO_chern_problem_geo
( ora_id_branch_by_kxd int
, ora_id_geosite int
)
insert into test2..OKNO_chern_problem_geo

select distinct a1.ora_id_branch_by_kxd, a1.ora_id_geosite from test2..OKNO_chern_whs_not_working a1
left join test2..OKNO_chern_whs_working a2 on a1.ora_id_geosite = a2.ora_id_geosite and a1.ora_id_branch_by_kxd = a2.ora_id_branch_by_kxd
where a2.count_whs_id is null

------------------------------------------------------------------------------------------------------------------------------

--Итоговая выгрузка
if OBJECT_ID ('test2..OKNO_chern_PA_novyeTT')is not null drop table test2..OKNO_chern_PA_novyeTT
create table test2..OKNO_chern_PA_novyeTT
(ora_id_ws int
,ora_dt_start date )

insert into test2..OKNO_chern_PA_novyeTT

select 
a1.ora_id_ws
,a4.ora_dt_start
from test2..OKNO_chern_whs_suag a1
join test2..OKNO_chern_problem_geo a2 on a1.ora_id_geosite = a2.ora_id_geosite and a1.ora_id_branch_by_kxd = a2.ora_id_branch_by_kxd
join TABLE_whs_vw a3 on a1.ora_id_ws = a3.ora_id_ws
left join test2..OKNO_whs_open a4 on a1.ora_id_ws = a4.ora_id_ws
where a3.reload_whs_name is not null
--select * from test2..OKNO_chern_PA_novyeTT

------------9 - определяем ГР-ТМ где ПА с учетом КА 100%        (возможно заменить на итоговую сверку-------------------------------------------)----------------------------------------
if OBJECT_ID ('test2..OKNO_PA_OK')is not null drop table test2..OKNO_PA_OK
create table test2..OKNO_PA_OK
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,TPinPA_sKA int
,TPinPAOK int )

insert into test2..OKNO_PA_OK

select distinct
a1.ora_id_ws ,a1.ora_id_groupart ,a1.ora_id_type_matrix , ch1.TPinPA_sKA
,case when ch1.TPinPA_sKA/a1.etalon = 1  then 1 else 0 end as TPinPAOK
from test2..OKNO_DA_ACA_PA_CMS  a1
left join test2..OKNO_PA_chernovik_TP_sKA ch1 on ch1.ora_id_ws = a1.ora_id_ws

-------------10 - ИТОГ---------------------------------------------------------------------
if OBJECT_ID ('test2..OKNO_DA_ACA_PA_itog0')is not null drop table test2..OKNO_DA_ACA_PA_itog0
create table test2..OKNO_DA_ACA_PA_itog0
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,ora_id_art int
,rule_uw_name varchar (300) 
,ora_id_rule_uw bigint
,koefAZ float
,inDA int
,inACA int
,inPA int
,inPAsKA int
,etalon float
,rang int 
,etalonTTnaGeoMax int
,lvl_geo varchar (50)
,TPinOM float
,TPinPA_sKA int
,TPinPAOK int
,klaster int
,novyeTT int
,Smena_grp int
)
insert into test2..OKNO_DA_ACA_PA_itog0

select distinct
 a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,a.ora_id_art
,a.rule_uw_name
,a.ora_id_rule_uw 
,a.koefAZ
,a.inDA
,a.inACA
,a.inPA
,a.inPAsKA
,a.etalon
,a.rang
,b6.etalonTTnaGeo as etalonTTnaGeoMax
,c.lvl_geo
,d.ART as TPinOM
,e.TPinPA_sKA 
,e.TPinPAOK      ----------------Эталон на ТТ заполнен
,a.klaster
,n.ora_id_ws as 'novyeTT'
,a.Smena_grp
from test2..OKNO_DA_ACA_PA_CMS a
left join test2..OKNO_TTnaGeoEtalonMax b6 on b6.ora_id_rule_uw = a.ora_id_rule_uw
left join test2..OKNO_chern_UrovenOM c on c.ora_id_rule_uw = a.ora_id_rule_uw
left join test2..OKNO_ACA_TP d on d.ora_id_rule_uw = a.ora_id_rule_uw
left join test2..OKNO_PA_OK e on e.ora_id_ws = a.ora_id_ws
                             and e.ora_id_groupart = a.ora_id_groupart
                             and e.ora_id_type_matrix = a.ora_id_type_matrix
left join test2..OKNO_chern_PA_novyeTT           n on n.ora_id_ws = a.ora_id_ws              
--            select count (a.td_art_id) from test2..OKNO_DA_ACA_PA_itog0 a

-------------11 - ИТОГ проставляем КЕЙСЫ---------------------------------------------------------------------
if OBJECT_ID ('test2..OKNO_DA_ACA_PA_itog1')is not null drop table test2..OKNO_DA_ACA_PA_itog1
create table test2..OKNO_DA_ACA_PA_itog1
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,ora_id_art int
,rule_uw_name varchar (300) 
,ora_id_rule_uw bigint
,koefAZ float
,inDA int
,inACA int
,inPA int
,inPAsKA int
,etalon float
,rang int 
,etalonTTnaGeoMax int
,lvl_geo varchar (300)
,TPinOM float
,TPinPA_sKA int
,TPinPAOK int
,klaster int
,KommentDlyaKM varchar (300)
,OMneNapolnen int
,NedoborVACA float 
,novyeTT varchar (300)
,TPinEtalon int
)
insert into test2..OKNO_DA_ACA_PA_itog1

select distinct
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
,a1.ora_id_art
,a1.rule_uw_name
,a1.ora_id_rule_uw 
,a1.koefAZ
,a1.inDA
,a1.inACA
,a1.inPA
,a1.inPAsKA
,a1.etalon
,a1.rang
,a1.etalonTTnaGeoMax
,a1.lvl_geo
,a1.TPinOM
,a1.TPinPA_sKA 
,a1.TPinPAOK    ----------------Эталон на ТТ заполнен
,a1.klaster
,case when a1.Smena_grp = 1 then 'ТП изменила ТГ, нужно корректировать АЦА'
                  when a1.TPinOM < a1.etalonTTnaGeoMax and a1.inPAsKA < a1.inPA then 'ОМ не наполнен. Добавить КА по ТП'
                  when a1.TPinOM < a1.etalonTTnaGeoMax and a1.inACA < a1.inDA then 'ОМ не наполнен. Добавить в АЦА'
                  when a1.TPinOM < a1.etalonTTnaGeoMax and a1.inDA < a1.inACA then 'ОМ не наполнен. Добавить в ДА'
                  when a1.TPinOM < a1.etalonTTnaGeoMax and a1.inPAsKA = a1.inPA and a1.inACA = a1.inDA then 'ОМ не наполнен. По этой ТП критики нет'
                  when a1.TPinOM >= a1.etalonTTnaGeoMax and a1.inPAsKA < a1.inPA then 'ОМ наполнен. Добавить КА по ТП'
                  when a1.TPinOM >= a1.etalonTTnaGeoMax and a1.inPAsKA = a1.inPA and a1.inACA = a1.inDA then 'ОМ наполнен. По этой ТП критики нет'
                  when a1.TPinOM >= a1.etalonTTnaGeoMax and a1.inDA < a1.inACA then 'ОМ наполнен. Добавить в ДА'
                  when a1.TPinOM >= a1.etalonTTnaGeoMax and a1.inACA < a1.inDA then 'ОМ наполнен. Добавить в АЦА'
                  else 'ошибка' end as KommentDlyaKM
,case when a1.TPinOM < a1.etalonTTnaGeoMax  then 1 else 0 end as OMneNapolnen
,case when a1.etalonTTnaGeoMax - a1.TPinOM <0 then 0 else a1.etalonTTnaGeoMax - a1.TPinOM end as NedoborVACA
,case when a1.novyeTT is not null then 'Первая ТТ на новой гео' else '-' end as novyeTT
,case when a1.rang <= a1.etalon then 1
      when a1.rang > a1.etalon then 2 
      when a1.etalon is null then 3
      else 0 end as TPinEtalon
from test2..OKNO_DA_ACA_PA_itog0 a1

-------------12 - ИТОГ считаем ТП по комменту для Отчета наполненности матрицы---------------------------------------------------------------------
if OBJECT_ID ('test2..OKNO_chern_DA_ACA_PA_kolvoTT2a')is not null drop table test2..OKNO_chern_DA_ACA_PA_kolvoTT2a
create table test2..OKNO_chern_DA_ACA_PA_kolvoTT2a
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,TP int
)
insert into test2..OKNO_chern_DA_ACA_PA_kolvoTT2a

select distinct
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix
,count (a1.ora_id_art)
from test2..OKNO_DA_ACA_PA_itog1 a1
where a1.KommentDlyaKM in ('ТП изменила ТГ, нужно корректировать АЦА')
and a1.TPinEtalon in (0,1)            
group by 
 a1.ora_id_ws
,a1.ora_id_groupart
,a1.ora_id_type_matrix

------------------------------------------------------------ОТЧЕТ  ОМ - ТП------------------------------------------------------------------------------------
/*
select 
 a3.reload_whs_name as 'РЦ гео', a3.branch_name as 'Филиал', a3.city_name as 'Город', a3.working as 'Работает', a3.frmt as 'Формат', a3.subfrmt as 'Субформат', a3.code as 'Код МХ', a3.store_name as 'МХ'
,a2.lvl0_name  as 'ГР20', a2.lvl1_name  as 'ГР21', a2.lvl2_name  as 'ГР22', a2.lvl3_name  as 'ГР23', a2.lvl4_name  as 'ГР24'
,a4.name as 'Тип матрицы'
,a1.rule_uw_name as 'ОМ'
,a1.lvl_geo  as 'Уровень ОМа'
,b.code as 'Код ТП' , b.name as 'ТП'
,a1.rang as 'Ранг'
,a1.koefAZ  as 'Коэф. АЗ'
,a1.klaster as 'Кластер'
,a1.etalon as 'Эталон'
,a1.inDA as 'в ДА'
,a1.inACA as 'в АЦА'
,a1.inPA as 'в ПА'
,a1.inPAsKA as 'в ПА с КА'
,a1.etalonTTnaGeoMax as 'Макимальный эталон на гео'
,a1.TPinOM as 'ТП в АЦА'
,a1.NedoborVACA as 'Недобор в АЦА'
,a1.TPinPA_sKA as 'ТП в ПА'
,a1.TPinPAOK  as 'ПА с КА закрывает эталон'
,a1.OMneNapolnen as '.ОМ не наполнен.'
,a1.KommentDlyaKM as 'Комментарий для категории'
,a1.novyeTT as 'ТТ на новой гео'
from test2..OKNO_DA_ACA_PA_itog1 a1
join dev_db..article_vw b            on b.ora_id_art = a1.ora_id_art
join dev_db..art_group_by_lvl2_vw a2 on a2.ora_id_groupart = a1.ora_id_groupart
join dev_db..whs_vw a3                                                              on a3.ora_id_ws = a1.ora_id_ws 
join dev_db..pe_matrix_type_vw a4    on a4.cms_id_type = a1.ora_id_type_matrix
--where not (a1.TPinPAOK = 1 and a1.KommentDlyaKM = 'Для инфо! 1.ОМ наполнен. 2. По этой ТП критики нет')  --Ислючаем ТТ с заполненным Эталоном и ТП без критики
where a1.KommentDlyaKM not in ('ОМ наполнен. По этой ТП критики нет', 'ОМ наполнен. Добавить в АЦА','ОМ не наполнен. По этой ТП критики нет')  --Ислючаем ТТ с заполненным Эталоном и ТП без критики
and a1.TPinEtalon = 1
order by  a3.store_name ASC, a1.rule_uw_name ASC, a1.rang ASC
*/


------------------------------------------------------------------Итог для Отчет по Коэф. АЗ - 25 .-------------------------------------------------------------------------------
---считаем ТТ по форматам
if OBJECT_ID ('test2..OKNO_PA_koefAZ0')is not null drop table test2..OKNO_PA_koefAZ0
create table test2..OKNO_PA_koefAZ0
(ora_id_groupart int
,ora_id_art int
,ora_id_controwner int
,td_frmt_id int
,TT int 
)
insert into test2..OKNO_PA_koefAZ0
select 
 a1.ora_id_groupart
,a1.ora_id_art
,a1.ora_id_controwner
,c.td_frmt_id
,count (distinct a1.ora_id_ws) as TT
from test2..OKNO_PA_chernovik a1
join TABLE_contractor_vw b on b.ora_id_contr = a1.ora_id_controwner
join TABLE_whs_vw c on c.ora_id_ws = a1.ora_id_ws
where name like 'РЦ%'
group by
 a1.ora_id_groupart
,a1.ora_id_art
,a1.ora_id_controwner
,c.td_frmt_id

---считаем коэф. без дублей по ТМ
if OBJECT_ID ('test2..OKNO_PA_koefAZ1')is not null drop table test2..OKNO_PA_koefAZ1
create table test2..OKNO_PA_koefAZ1
(ora_id_groupart int
,ora_id_art int
,ora_id_controwner int
,td_frmt_id int
,ora_id_ws int
,koefAZ float
)
insert into test2..OKNO_PA_koefAZ1

select distinct
a1.ora_id_groupart
,a1.ora_id_art
,a1.ora_id_controwner
,c.td_frmt_id
,a1.ora_id_ws
,a1.koefAZ 
from test2..OKNO_PA_chernovik a1
join TABLE_contractor_vw b on b.ora_id_contr = a1.ora_id_controwner
join dev_db..whs_vw c on c.ora_id_ws = a1.ora_id_ws
where name like 'РЦ%'

-----Отчет
if OBJECT_ID ('test2..OKNO_PA_koefAZ')is not null drop table test2..OKNO_PA_koefAZ
create table test2..OKNO_PA_koefAZ
(ora_id_groupart int
,ora_id_art int
,ora_id_controwner int
,MD int
,GM int
,MK int
,MA int
,PR int
,koefAZ float 
)
insert into test2..OKNO_PA_koefAZ

select 
 a1.ora_id_groupart
,a1.ora_id_art
,a1.ora_id_controwner
,case when c1.TT is not null then c1.TT else 0 end as MD
,case when c2.TT is not null then c2.TT else 0 end as BF
,case when c3.TT is not null then c3.TT else 0 end as MK
,case when c4.TT is not null then c4.TT else 0 end as MA
,case when c5.TT is not null then c5.TT else 0 end as PR
,sum (a1.koefAZ) as koefAZ
from test2..OKNO_PA_koefAZ1 a1
join dev_db..contractor_vw b on b.ora_id_contr = a1.ora_id_controwner
left join test2..OKNO_PA_koefAZ0 c1 on c1.ora_id_controwner = a1.ora_id_controwner and c1.ora_id_art = a1.ora_id_art and c1.td_frmt_id = 1
left join test2..OKNO_PA_koefAZ0 c2 on c2.ora_id_controwner = a1.ora_id_controwner and c2.ora_id_art = a1.ora_id_art and c2.td_frmt_id = 2
left join test2..OKNO_PA_koefAZ0 c3 on c3.ora_id_controwner = a1.ora_id_controwner and c3.ora_id_art = a1.ora_id_art and c3.td_frmt_id = 3
left join test2..OKNO_PA_koefAZ0 c4 on c4.ora_id_controwner = a1.ora_id_controwner and c4.ora_id_art = a1.ora_id_art and c4.td_frmt_id = 104
left join test2..OKNO_PA_koefAZ0 c5 on c5.ora_id_controwner = a1.ora_id_controwner and c5.ora_id_art = a1.ora_id_art and c5.td_frmt_id = 47
where name like 'РЦ%'
group by
 a1.ora_id_groupart
,a1.ora_id_art
,a1.ora_id_controwner
,c1.TT ,c2.TT ,c3.TT ,c4.TT ,c5.TT

-------------МО-------------------------------------------------------------------
IF OBJECT_ID('test2..OKNO_chern_PA_status_mo') IS NOT NULL DROP TABLE test2..OKNO_chern_PA_status_mo;
create table test2..OKNO_chern_PA_status_mo
(ora_id_art        int
,ora_id_contr    int
,ora_id_mo_rc bigint
)
insert into test2..OKNO_chern_PA_status_mo

select distinct
                 a3.ora_id_art
                ,a5.ora_id_contr
                ,max(a1.ora_id_mo_rc)
from TABLE_mo_rc_tbl a1 --МО
join TABLE_article_vw a3       on a3.ora_id_art = a1.ora_id_art
join TABLE_contractor_vw a5    on a5.ora_id_contr = a1.ora_id_rc
join test2..OKNO_PA_koefAZ a on a.ora_id_art = a3.ora_id_art and a.ora_id_controwner = a5.ora_id_contr
where a1.dt_end is null                                   
group by a3.ora_id_art, a5.ora_id_contr

---------------------МО2---------------
IF OBJECT_ID('test2..OKNO_chern_PA_status_mo1') IS NOT NULL DROP TABLE test2..OKNO_chern_PA_status_mo1;
create table test2..OKNO_chern_PA_status_mo1
(ora_id_art        int
,ora_id_contr int
,status int
)
insert into test2..OKNO_chern_PA_status_mo1
select distinct
                a3.ora_id_art,
                a5.ora_id_contr,
                a1.status
from TABLE_mo_rc_tbl a1
join TABLE_article_vw a3       on a3.ora_id_art = a1.ora_id_art
join TABLE_contractor_vw a5    on a5.ora_id_contr = a1.ora_id_rc
join test2..OKNO_chern_PA_status_mo a on a.ora_id_art = a3.ora_id_art and a.ora_id_contr = a5.ora_id_contr and a.ora_id_mo_rc = a1.ora_id_mo_rc
where a1.dt_end is null                                   

---------------------------- Отчет по Коэф. АЗ - 25 .-----------------------------
/*
select  
 a2.lvl0_name  as 'ГР20', a2.lvl1_name  as 'ГР21', a2.lvl2_name  as 'ГР22', a2.lvl3_name  as 'ГР23', a2.lvl4_name  as 'ГР24'
,a31.code as 'Код ТП' ,a31.name as 'ТП'
,b.code as 'Код КА'
,b.name as 'КА'
,m1.catalog_value as 'Статус МО'
,a1.GM as 'ГМ',a1.MD as 'МД', a1.MK as 'МК' ,a1.MA as 'МА',a1.PR as 'ПР'
,a1.koefAZ as 'Коэффициент подключения'
from test2..OKNO_PA_koefAZ a1
join TABLE_art_group_by_lvl2_vw a2 on a1.ora_id_groupart = a2.ora_id_groupart
join TABBLE_article_vw a31           on a31.ora_id_art = a1.ora_id_art
join TABLE_contractor_vw b on b.ora_id_contr = a1.ora_id_controwner and b.name  like 'РЦ%'
    left join test2..OKNO_chern_PA_status_mo1 c on c.ora_id_art = a1.ora_id_art and c.ora_id_contr = a1.ora_id_controwner
    left join TABLE_value_tbl m1 on m1.id_catalog = c.status and m1.dt_end is null and m1.catalog_name = 'status_mo'
where a1.koefAZ < 25
*/

--------------------------------------------------------ПД-------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID('test2..OKNO_PD_PZ') IS NOT NULL drop table test2..OKNO_PD_PZ
create table test2..OKNO_PD_PZ
                (ora_id_art int
                ,ora_id_ws int)
insert test2..OKNO_PD_PZ

select 
a.ora_id_art, a.ora_id_ws
from TABLE_add_entity_tbl a
join (select distinct ora_id_art from test2..OKNO_ACA a) b on b.ora_id_art = a.ora_id_art
where a.dt_end is null
and a.sales is null

--                                            
IF OBJECT_ID('test2..OKNO_PD_PZ2') IS NOT NULL DROP TABLE test2..OKNO_PD_PZ2;
create table test2..OKNO_PD_PZ2
                (ora_id_art int,
                 td_frmt_id int, 
                 reload_whs_id int )
insert test2..OKNO_PD_PZ2
select distinct
                  a.ora_id_art
                , b1.td_frmt_id
                , b1.reload_whs_id
from test2..OKNO_PA_chernovik a
join TABLE_whs_vw b1 on b1.ora_id_ws = a.ora_id_ws
join TABLE_store_tbl b on  b.ora_id_ws = b1.ora_id_ws
                       and b.dt_end is null 
                       and b.ora_id_status_ws in (3,4)
where  exists (select 1 from test2..OKNO_PD_PZ b
               where b.ora_id_ws = a.ora_id_ws
               and b.ora_id_art = a.ora_id_art)

--------------Отчет - ПД ПЗ              до ТП                    
/*
select
                  agp.lvl0_name as [ТГ20]
                , agp.lvl1_name as [ТГ21]
                , agp.lvl2_name as [ТГ22]
                , agp.lvl3_name as [ТГ23]
                , art.code as [Код ТП]
                , art.name as [Название ТП]
                , f.frmt_name as [Формат]
                , rc.reload_whs_name as [РЦ]
from test2..OKNO_PD_PZ2 sa
join TABLE_article_vw art         on art.ora_id_art = sa.ora_id_art
join (select distinct a.td_frmt_id, frmt_name from dev_db..formats_vw a) f        on f.td_frmt_id = sa.td_frmt_id
join TABLErc_vw rc    on rc.reload_whs_id = sa.reload_whs_id
join TABLE_art_group_by_lvl2_vw agp               on agp.id = art.id_art_group
*/

-----------------------------------------------------              Контроль транзакций АЗ -------------------------------------------------------------------------------------------------

----------------------------Контроль транзакций АЗ--Ограничиваем Статусы--------
if OBJECT_ID ('test2..OKNO_Oshibki_KM')is not null drop table test2..OKNO_Oshibki_KM
create table test2..OKNO_Oshibki_KM
(ora_id_azrc_err_state int
,name_err_state varchar (200)
)
insert into test2..OKNO_Oshibki_KM
select distinct  
 a.ora_id_azrc_err_state
,a.name_err_state
from TABLE_state_ref_tbl  a
where a.name_err_state in ('Обработка ротационной транзакции на вывод с РЦ приостановлена, т.к. парная транзакция не прошла входной контроль, в карточке товара статус "Местный" (46)','Обработка транзакции на ввод приостановлена, в карточке товара статус "Местный"','Ссылка отклонена, нарушен минимальный порог подключения')
and a.dt_end is null

----------------------Контроль транзакций АЗ---Выгружаем Команды-------------------------------------------------
if OBJECT_ID ('test2..OKNO_komandy')is not null drop table test2..OKNO_komandy
create table test2..OKNO_komandy
(ora_id_art         int
,ora_id_contr       int
,StatusTT           varchar (200)
,ora_id_azrc_status varchar (200)
,type               varchar (200)
,status             varchar (200)
,ora_id_doc         varchar (200)
,dt_from            date
,dt_end             date
,name_err_state     varchar (200)
,ora_id_ws          int
)
insert into test2..OKNO_komandy
select
a.ora_id_art
,a.ora_id_contr
,case when a5.ora_id_status_ws = 1 then 'Новая'
                  when a5.ora_id_status_ws = 3 then 'Редизайн'
                  when a5.ora_id_status_ws = 4 then 'Действующая'
                  when a5.ora_id_status_ws = 5 then 'Недействующая' end as StatusTT
,a.ora_id_azrc_status
,a.type
,a.status
,a.ora_id_doc
,a.dt_from
,a.dt_end
,b.name_err_state
,a.ora_id_ws
from TABLE_commands_tbl a
join (select distinct ora_id_art from test2..OKNO_ACA a) e on e.ora_id_art = a.ora_id_art --ограничиваем ТП из релиза
left join TABLE_to_suag_tbl a1 on a1.ora_id_doc = a.ora_id_doc  and a1.dt_end is null --and a1.ora_id_azrc_err_state in (24,46,60) 
join test2..OKNO_Oshibki_KM b on b.ora_id_azrc_err_state = a1.ora_id_azrc_err_state  --ограничиваем на ошибки
join  TABLE_store_tbl  a5 on a5.ora_id_ws = a.ora_id_ws and a5.dt_end is null
where a.dt_end is null
and a.status in ('I')  --I' в обработке,'A' обработано

--------------------Контроль транзакций АЗ------Итоговый отчет-----------
/*
select a.ora_id_doc as 'Транзакция'
,case when a.type='IN' then 'Ввод' 
                  when a.type='OUT' then 'Вывод' else 'Проверить' end as 'Операция'
,a.name_err_state as 'Ошибка от АЗ'
,f.code as 'Код КА'
,f.name as 'КА'
,a3.code as 'Код ТП'
,a3.name as 'ТП'
,a4.lvl0_name as 'Гр20'
,a4.lvl1_name as 'Гр21'
,a4.lvl2_name as 'Гр22'
,a4.lvl3_name as 'Гр23'
,b.reload_whs_name as 'РЦ'
,b.frmt as 'Формат'
,b.branch_name as 'Филиал'
,b.code as 'Код ТТ'
,b.store_name  as 'ТТ'
,a.StatusTT  as 'Статус ТТ'
from test2..OKNO_komandy a
join TABLE_whs_vw b on b.ora_id_ws = a.ora_id_ws
join TABLE_article_vw a3 on a3.ora_id_art = a.ora_id_art
join TABLE_contractor_vw f on f.ora_id_contr = a.ora_id_contr
join TABLE_art_group_by_lvl2_vw a4 on a4.Td_grp_art_id = a3.Td_grp_art_id
*/

----------------------------------------------ЗАПРЕТЫ---------Задаём ограничения по Дате запрета---------------------------------------------------------------------
IF OBJECT_ID('test2..OKNO_date_for_zapret') IS NOT NULL
DROP TABLE test2..OKNO_date_for_zapret;
create table test2..OKNO_date_for_zapret
( ora_id_ws int
, ora_id_art int
, ora_dt_begin date
, ora_dt_end date
, delta int
)
insert into test2..OKNO_date_for_zapret

select  
  a1.ora_id_ws
, b.ora_id_art
, a.ora_dt_begin
, a.ora_dt_end 
, datediff (day, a.ora_dt_begin, a.ora_dt_end) as delta
from TABLE_banned_assort_vw a
join TABLE_whs_vw a1 on a1.td_whs_id = a.td_whs_id
join TABLE_article_vw b on b.td_art_id = a.td_art_id
join (select distinct ora_id_art from test2..OKNO_ACA a) e on e.ora_id_art = b.ora_id_art --ограничиваем ТП из релиза
group by 
  a1.ora_id_ws
, b.ora_d_art
, a.ora_dt_begin
, a.ora_dt_end 
having datediff (day, ora_dt_begin, ora_dt_end) > 14
 
------------Проверяем наличие в идеальной матрице--------------
IF OBJECT_ID('test2..OKNO_im_zapret') IS NOT NULL DROP TABLE test2..OKNO_im_zapret;
create table test2..OKNO_im_zapret
( ora_id_ws int
, ora_id_art int
, ora_dt_begin date
, ora_dt_end date
, delta int
)
insert into test2..OKNO_im_zapret

select distinct
  a.ora_id_ws
, a.ora_id_art
, a.ora_dt_begin
, a.ora_dt_end 
, a.delta
from test2..OKNO_date_for_zapret a
join test2..OKNO_PA_chernovik b on a.ora_id_ws = b.ora_id_ws and a.ora_id_art = b.ora_id_art 

---------ЗАПРЕТЫ----Отчет--------------
/*
select 
  a2.reload_whs_name as 'РЦ гео'
, a2.branch_name as 'Филиал'
, a2.city_name as 'Город'
, a2.frmt as 'Формат'
, a2.subfrmt as 'Субформат'
, a2.code   as 'Код МХ'
, a2.store_name as 'МХ'
, a4.lvl0_name as 'Гр20', a4.lvl1_name as 'Гр21', a4.lvl2_name as 'Гр22', a4.lvl3_name as 'Гр23', a4.lvl4_name as 'Гр24'
, a3.code as 'Код ТП', a3.name as 'ТП'
, a1.ora_dt_begin as 'Дата с'
, a1.ora_dt_end as 'Дата по'
, a1.delta as 'Дней в запрете'
from test2..OKNO_im_zapret a1
join TABLE_whs_vw a2                       on a2.ora_id_ws = a1.ora_id_ws
join TABLE_article_vw a3                   on a3.ora_id_art = a1.ora_id_art
join TABLE_art_group_by_lvl2_vw a4         on a4.Td_grp_art_id = a3.Td_grp_art_id
*/

----------------------------------------------------------------------------------ОТЧЕТ НОВЫЕ ТТ--------------------------------------------------------------------
-----ОТЧЕТ НОВЫЕ ТТ--отчет до ТТ-ОМ--
/*
select distinct a3.reload_whs_name as 'РЦ гео', a3.branch_name as 'Филиал', a3.city_name as 'Город', a3.working as 'Работает', a3.frmt as 'Формат', a3.subfrmt as 'Субформат', a3.code as 'Код МХ', a3.store_name as 'МХ'
,a2.lvl0_name  as 'ГР20', a2.lvl1_name  as 'ГР21', a2.lvl2_name  as 'ГР22', a2.lvl3_name  as 'ГР23', a2.lvl4_name  as 'ГР24'
,a4.name as 'Тип матрицы', a1.etalon as 'Эталон'
,om.rule_uw_name as 'ОМ'
,case when t.ora_id_ws is not null then 'Первая ТТ на новой гео' else '-' end as 'ТТ на новой гео'
,d.TP as 'ТП в ДА'
,p.ART as 'ТП в ОМ'
,ch.TPinPA  as 'ТП в ПА, без учета КА'
,ch1.TPinPA_sKA  as 'ТП в ПА, с учетом КА'
,case when isnull(ch.TPinPA,0) = a1.etalon and ch1.TPinPA_sKA = ch.TPinPA then 'ПА наполнен'
                  when p.ART <  a1.etalon and isnull(ch.TPinPA,0) <= p.ART    and isnull(ch1.TPinPA_sKA,0) < isnull(ch.TPinPA,0) then 'Добавить АЦА, Добавить ДА, Добавить КА' 
                  when p.ART <  a1.etalon and isnull(ch.TPinPA,0) <= p.ART    and isnull(ch1.TPinPA_sKA,0) = isnull(ch.TPinPA,0) then 'Добавить АЦА, Добавить ДА' 
                  when p.ART >= a1.etalon and isnull(ch.TPinPA,0) < a1.etalon and isnull(ch1.TPinPA_sKA,0) < isnull(ch.TPinPA,0) then 'Добавить ДА, Добавить КА' 
                  when p.ART >= a1.etalon and isnull(ch.TPinPA,0) = a1.etalon and isnull(ch1.TPinPA_sKA,0) < isnull(ch.TPinPA,0) then 'Добавить КА' 
                  when p.ART >= a1.etalon and isnull(ch.TPinPA,0) < a1.etalon and isnull(ch1.TPinPA_sKA,0) = isnull(ch.TPinPA,0) then 'Добавить ДА' 
                  when isnull(ch.TPinPA,0) < a1.etalon and isnull(ch1.TPinPA_sKA,0) < isnull(ch.TPinPA,0) then 'Добавить ДА, Добавить КА' 
                  when isnull(ch1.TPinPA_sKA,0) < ch.TPinPA then 'Добавить КА'
                  when isnull(ch.TPinPA,0) < a1.etalon and isnull(ch1.TPinPA_sKA,0) = isnull(ch.TPinPA,0) then 'Добавить ДА'  
                  when om.rule_uw_name is null then 'Нет Ома'
                  when ch.TPinPA is null then 'В ПА 0 ТП'
                  else 'ошибка' end as 'Комментарий для категории'
from test2..OKNO_DA_ACA_PA_CMS a1
join TABLE_art_group_by_lvl2_vw a2 on a2.ora_id_groupart = a1.ora_id_groupart
join TABLE_whs_vw a3                                                              on a3.ora_id_ws = a1.ora_id_ws 
join TABLE_pe_matrix_type_vw a4    on a4.cms_id_type = a1.ora_id_type_matrix
left join test2..OKNO_chern_PA_novyeTT t on t.ora_id_ws = a1.ora_id_ws
left join test2..OKNO_TT_OM om on om.ora_id_ws   = a1.ora_id_ws
                              and om.ora_id_groupart    = a1.ora_id_groupart
                              and om.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_DATT_TP d on d.ora_id_ws = a1.ora_id_ws
                               and d.ora_id_groupart = a1.ora_id_groupart
left join test2..OKNO_ACA_TP p on p.ora_id_rule_uw = om.ora_id_rule_uw
left join test2..OKNO_PA_chernovik_TP ch on ch.ora_id_ws = a1.ora_id_ws
                                        and ch.ora_id_groupart = a1.ora_id_groupart
                                        and ch.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_PA_chernovik_TP_sKA ch1 on ch1.ora_id_ws = a1.ora_id_ws
                                             and ch1.ora_id_groupart = a1.ora_id_groupart
                                             and ch1.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_PA_OK c1 on c1.ora_id_ws = a1.ora_id_ws
                              and c1.ora_id_groupart = a1.ora_id_groupart
                              and c1.ora_id_type_matrix = a1.ora_id_type_matrix
where t.ora_id_ws is not null   --показываем только новые ТТ
and c1.TPinPAOK = 0
and a1.etalon > 0
*/

--------------------------------------------------Локально - Федеральные ТП-------------------------------------------------------------------------------------------------
/*
select 
 a2.lvl0_name as 'Гр20', a2.lvl1_name as 'Гр21', a2.lvl2_name as 'Гр22', a2.lvl3_name as 'Гр23', a2.lvl4_name as 'Гр24'
,a.art_code as 'Код ТП',a.art_name as 'ТП'
,a.art_type as 'Локальный/Федеральный'
from TABLE_arttype_local_federal_static_vw a
join TABLE_article_vw b                            on b.td_art_id = a.td_art_id
join TABLE_art_group_by_lvl2_vw a2 on a2.td_grp_art_id = b.td_grp_art_id
join (select distinct ora_id_art from test2..OKNO_ACA a) e on e.ora_id_art = a.ora_id_art --ограничиваем ТП из релиза
order by a.art_type
*/

-----------------------------------Логистические возможности МП---------------------------
/*
select  a.* 
from TABLE_draftPa_vw a--- покрытие черновика ПА
join TABLE_article_vw b                              on b.code = a.[Код ТП]
join (select distinct ora_id_art from test2..OKNO_ACA a) e on e.ora_id_art = b.ora_id_art --ограничиваем ТП из релиза
where a.[Ответственный за направление] = 'ДРП, по КА не задана логистика в СУАГ'
*/

------------------------------------Наполненость матрицы + Индекс здоровья-------------------------------------------------------------------------------------------------

---------------------------------ПА---------------------------
if OBJECT_ID ('test2..OKNO_Matr_IM_napolnenie')is not null drop table test2..OKNO_Matr_IM_napolnenie
create table test2..OKNO_Matr_IM_napolnenie
(ora_id_ws int
,ora_id_art int
,ora_id_groupart int
,td_cntr_id int
,ora_id_type_matrix int
,is_normalized int
,normalized_azrc int
,koefAZ int
,zapret int
,PD_PZ int
,status_MO varchar (100) 
)
insert into test2..OKNO_Matr_IM_napolnenie
select distinct
a.ora_id_ws
,a.ora_id_art
,a.ora_id_groupart
,a.ora_id_controwner
,a.ora_id_type_matrix
,case when a.ora_id_groupart = 403 then u1.is_normalized else b.is_normalized end as is_normalized     -----для ТГ21 сигареты нормализация по МРЦ
,case when a.ora_id_controwner is null or a5.name not like 'РЦ%' then 1
      when b.normalized_azrc = 1 then b.normalized_azrc else 0 end as normalized_azrc
,case when c.ora_id_art is null then 0 else 1 end as koefAZ
,case when d.ora_id_art is null then 0 else 1 end as zapret
,case when e.ora_id_art is null then 0 else 1 end as PD_PZ
,case when a5.name not like 'РЦ%' then 'Есть МО' 
      when m1.catalog_value in ('Действующее МО') then 'Есть МО' else 'Нет МО' end as status_MO
from test2..OKNO_PA_chernovik a
left join TABLE_article_tbl b           on b.ora_id_art = a.ora_id_art and b.dt_end is null
left join TABLE_contractor_vw a5        on a5.ora_id_contr = a.ora_id_controwner
left join test2..OKNO_PA_koefAZ c                           on c.ora_id_art = a.ora_id_art and c.ora_id_controwner = a.ora_id_controwner and c.koefAZ < 25
left join test2..OKNO_im_zapret d                            on d.ora_id_art = a.ora_id_art and d.ora_id_ws = a.ora_id_ws
left join test2..OKNO_PD_PZ  e                       on e.ora_id_art = b.ora_id_art and  e.ora_id_ws = a.ora_id_ws
left join test2..OKNO_chern_PA_status_mo1 c1                on c1.ora_id_art = a.ora_id_art and c1.ora_id_contr = a.ora_id_controwner
left join TABLE_value_tbl m1 on m1.id_catalog = c1.status and m1.dt_end is null and m1.catalog_name = 'status_mo'
left join TABLE_rc_tbl u on u.ora_id_utp = b.ora_id_art and u.ora_id_contr = a5.ora_id_contr and u.dt_end is null
left join TABLE_article_tbl u1         on u1.ora_id_art = u.ora_id_mrc and u1.dt_end is null
/*
select distinct (a.status_MO)
from test2..OKNO_chern_Matr_IM_napolnenie a
*/

---------------------------------Идеальная матрица считаем ТП без МО ---------------------------
if OBJECT_ID ('test2..OKNO_Matr_IM_napolnenie_net_MO')is not null drop table test2..OKNO_Matr_IM_napolnenie_net_MO
create table test2..OKNO_Matr_IM_napolnenie_net_MO
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,art int
)
insert into test2..OKNO_Matr_IM_napolnenie_net_MO

select 
 a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,count (a.ora_id_art)
from test2..OKNO_Matr_IM_napolnenie a
where a.status_MO in ('Нет МО')
group by
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix

---------------------------------Идеальная матрица считаем ТП по НЕНОРМАЛИЗОВАННЫМ ---------------------------
if OBJECT_ID ('test2..OKNO_chern_Matr_IM_napolnenie_nenorm')is not null drop table test2..OKNO_chern_Matr_IM_napolnenie_nenorm
create table test2..OKNO_chern_Matr_IM_napolnenie_nenorm
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,art int
)
insert into test2..OKNO_chern_Matr_IM_napolnenie_nenorm

select
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,count (a.ora_id_art)
from test2..OKNO_Matr_IM_napolnenie a
where a.is_normalized = 0
group by
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix


---------------------------------Идеальная матрица считаем ТП по НЕНОРМАЛИЗОВАННЫМ для АЗ ---------------------------
if OBJECT_ID ('test2..OKNO_Matr_IM_napolnenie_nenorm_AZ')is not null drop table test2..OKNO_Matr_IM_napolnenie_nenorm_AZ
create table test2..OKNO_Matr_IM_napolnenie_nenorm_AZ
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,art int
)

insert into test2..OKNO_Matr_IM_napolnenie_nenorm_AZ
select 
 a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,count (a.ora_id_art)
from test2..OKNO_Matr_IM_napolnenie a
where a.normalized_azrc = 0
group by
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
 

---------------------------------Идеальная матрица считаем ТП по koefAZ ---------------------------
if OBJECT_ID ('test2..OKNO_Matr_IM_napolnenie_koefAZ')is not null drop table test2..OKNO_Matr_IM_napolnenie_koefAZ
create table test2..OKNO_Matr_IM_napolnenie_koefAZ
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,art int
)
insert into test2..OKNO_Matr_IM_napolnenie_koefAZ

select 
 a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,count (a.ora_id_art)
from test2..OKNO_Matr_IM_napolnenie a
where a.koefAZ = 1
group by
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix


---------------------------------Идеальная матрица считаем ТП по zapret ---------------------------
if OBJECT_ID ('test2..OKNO_Matr_IM_napolnenie_zapret')is not null drop table test2..OKNO_Matr_IM_napolnenie_zapret
create table test2..OKNO_Matr_IM_napolnenie_zapret
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,art int
)

insert into test2..OKNO_Matr_IM_napolnenie_zapret
select 
 a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,count (a.ora_id_art)
from test2..OKNO_Matr_IM_napolnenie a
where a.zapret = 1
group by
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix

---------------------------------Идеальная матрица считаем ТП по PD_PZ ---------------------------
if OBJECT_ID ('test2..OKNO_Matr_IM_napolnenie_PD_PZ')is not null drop table test2..OKNO_Matr_IM_napolnenie_PD_PZ
create table test2..OKNO_Matr_IM_napolnenie_PD_PZ
(ora_id_ws int
,ora_id_groupart int
,ora_id_type_matrix int
,art int
)
insert into test2..OKNO_Matr_IM_napolnenie_PD_PZ

select
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix
,count (a.ora_id_art)
from test2..OKNO_Matr_IM_napolnenie a
where a.PD_PZ = 1
group by
a.ora_id_ws
,a.ora_id_groupart
,a.ora_id_type_matrix

--------------------------ОТЧЕТ-------------
/*
select distinct
a3.reload_whs_name as 'РЦ', a3.branch_name as 'Филиал', a3.city_name as 'Город', a3.working as 'Работает', a3.frmt as 'Формат', a3.subfrmt as 'Субформат', a3.code as 'Код МХ', a3.store_name as 'МХ'
,a2.lvl0_name  as 'ГР20', a2.lvl1_name  as 'ГР21', a2.lvl2_name  as 'ГР22', a2.lvl3_name  as 'ГР23', a2.lvl4_name  as 'ГР24'
,a4.name as 'Тип матрицы', a1.etalon as 'Эталон'
                ,case when i.TPinPA is null then 0 else i.TPinPA end  as 'ТП в Подобранном Ассортименте'
                ,case when i1.TPinPA_sKA is null then 0 else i1.TPinPA_sKA end  as 'ТП в Подобранном Ассортименте с КА'
                ,case when n.art is null then 0 else n.art end  as 'ТП Ненормализована'
                ,case when n1.art is null then 0 else n1.art end  as 'ТП Ненормализована для АЗ РЦ'
                ,case when k.art is null then 0 else k.art end  as 'ТП не проходят коэффициент АЗ'
                ,case when p.art is null then 0 else p.art end  as 'По ТП нет ПД'
                ,case when z.art is null then 0 else z.art end  as 'ТП в запретеболее 14 дней'
                ,case when g.TP is null then 0 else g.TP end  as 'ТП изменили группу'
                ,a1.rule_uw_name as 'ОМ'
                ,case when m.art is null then 0 else m.art end  as 'Нет МО'
from test2..OKNO_DA_ACA_PA_CMS a1
join TABLE_art_group_by_lvl2_vw a2 on a1.ora_id_groupart = a2.ora_id_groupart
join TABLE_whs_vw a3                                                              on a1.ora_id_ws = a3.ora_id_ws 
join TABLE_pe_matrix_type_vw a4    on a4.cms_id_type = a1.ora_id_type_matrix
left join test2..OKNO_PA_chernovik_TP i on i.ora_id_ws=a1.ora_id_ws
                                  and i.ora_id_groupart=a1.ora_id_groupart
                                  and i.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_PA_chernovik_TP_sKA i1 on i1.ora_id_ws=a1.ora_id_ws
                                            and i1.ora_id_groupart=a1.ora_id_groupart
                                            and i1.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_Matr_IM_napolnenie_net_MO m  on m.ora_id_ws=a1.ora_id_ws
                                                  and m.ora_id_groupart=a1.ora_id_groupart
                                                  and m.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_chern_Matr_IM_napolnenie_nenorm n on n.ora_id_ws=a1.ora_id_ws
                                                  and n.ora_id_groupart=a1.ora_id_groupart
                                                  and n.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_Matr_IM_napolnenie_nenorm_AZ n1 on n1.ora_id_ws=a1.ora_id_ws
                                                     and n1.ora_id_groupart=a1.ora_id_groupart
                                                     and n1.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_Matr_IM_napolnenie_koefAZ k on k.ora_id_ws=a1.ora_id_ws
                                                 and k.ora_id_groupart=a1.ora_id_groupart
                                                 and k.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_Matr_IM_napolnenie_PD_PZ p  on p.ora_id_ws=a1.ora_id_ws
                                                 and p.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_Matr_IM_napolnenie_zapret z  on z.ora_id_ws=a1.ora_id_ws
                                                  and z.ora_id_groupart = a1.ora_id_groupart
                                                   and z.ora_id_type_matrix = a1.ora_id_type_matrix
left join test2..OKNO_chern_DA_ACA_PA_kolvoTT2a g on g.ora_id_ws=a1.ora_id_ws
                                                 and g.ora_id_groupart=a1.ora_id_groupart
                                                 and g.ora_id_type_matrix = a1.ora_id_type_matrix 
*/                           

 

 